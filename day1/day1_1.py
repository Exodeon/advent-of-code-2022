current_max = 0
current_sum = 0
try:
    while True:
        current_input = input()
        if current_input == "":
            if current_sum > current_max:
                current_max = current_sum
            current_sum = 0
        else:
            current_sum += int(current_input)

except EOFError:
    print("Max = ", max(current_max, current_sum))
