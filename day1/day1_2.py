import heapq

current_sum = 0
calories_sum = []
try:
    while True:
        current_input = input()
        if current_input == "":
            calories_sum.append(current_sum)
            current_sum = 0
        else:
            current_sum += int(current_input)

except EOFError:
    print(sum(heapq.nlargest(3, calories_sum)))
