#include <iostream>
#include <sstream>

int main() {
    std::string line;
    int pairsIncluded = 0;
    while (std::cin >> line) {
        // parsing inputs
        // separating 2 pairs
        std::stringstream stringStream(line);
        std::string pair1;
        std::string pair2;
        getline(stringStream, pair1, ',');
        getline(stringStream, pair2, ',');

        // get each bound of pair
        char separator;
        // pair 1
        unsigned int pair1min, pair1max;
        std::stringstream pair1stream(pair1);
        pair1stream >> pair1min >> separator >> pair1max;

        // pair 2
        unsigned int pair2min, pair2max;
        std::stringstream pair2stream(pair2);
        pair2stream >> pair2min >> separator >> pair2max;

        if ((pair1min <= pair2min and pair1max >= pair2max) or (pair1min >= pair2min and pair1max <= pair2max)) {
            pairsIncluded++;
        }
    }
    std::cout << pairsIncluded << std::endl;
}