#include <iostream>
#include <vector>

bool isInGrid(int x, int y, int size) {
    return x >= 0 and x < size and y >= 0 and y < size;
}

int main() {
    // parse input
    std::vector<std::vector<int>> trees;
    std::string line;
    while (std::cin >> line) {
       std::vector<int> treeLine;
        for (auto& c : line) {
            treeLine.push_back(c - '0');
        }
        trees.push_back(treeLine);
    }

    int maxScenicScore = 0;

    std::vector<std::pair<int,int>> directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    // don't check edges because viewing distance is always 0
    for (int y = 1; y < trees.size() - 1; y++) {
        // same size because it's a square
        for (int x = 1; x < trees.size() - 1; x++) {
            int scenicScore = 1;
            for (auto& direction: directions) {
                int viewingDistance = 0;
                bool visible = true;
                int currentX = x + direction.first;
                int currentY = y + direction.second;
                while (visible and isInGrid(currentX, currentY, trees.size())) {
                    if (trees[currentY][currentX] >= trees[y][x]) {
                        visible = false;
                    }
                    viewingDistance++; // last tree seen counts in viewing distance
                    currentX += direction.first;
                    currentY += direction.second;
                }
                scenicScore *= viewingDistance;
            }
            if (scenicScore > maxScenicScore) {
                maxScenicScore = scenicScore;
            }

        }

    }

    std::cout << "Max scenic score : " << maxScenicScore << std::endl;
}