#include <iostream>
#include <vector>

bool isInGrid(int x, int y, int size) {
    return x >= 0 and x < size and y >= 0 and y < size;
}

int main() {
    // parse input
    std::vector<std::vector<int>> trees;
    std::string line;
    while (std::cin >> line) {
       std::vector<int> treeLine;
        for (auto& c : line) {
            treeLine.push_back(c - '0');
        }
        trees.push_back(treeLine);
    }

    int numberVisibles = 0;
    std::vector<std::pair<int,int>> directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    for (int y = 0; y < trees.size(); y++) {
        // same size because it's a square
        for (int x = 0; x < trees.size(); x++) {
            for (auto& direction: directions) {
                bool visible = true;
                int currentX = x + direction.first;
                int currentY = y + direction.second;
                while (visible and isInGrid(currentX, currentY, trees.size())) {
                    if (trees[currentY][currentX] >= trees[y][x]) {
                        visible = false;
                    }
                    currentX += direction.first;
                    currentY += direction.second;
                }
                // it is enough to be visible in one direction
                if (visible) {
                    numberVisibles++;
                    break; // TODO
                }
            }
        }
    }

    std::cout << "Number of visibles : " << numberVisibles << std::endl;
}