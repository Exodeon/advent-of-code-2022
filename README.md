# Advent of Code 2022



[Link to Advent of Code](https://adventofcode.com/2022)



I am doing the challenge in C++, except for the first day which I did in Python.



Each program expects an input file in standard input. The easiest way to run the programs is to be in the exercise directory. Here's an example for Python :

```bash 
python3 day1_1.py < day1_input
```

And here's an example for C++ : 

```bash
g++ --std=c++20 day2_1.cpp -o day2_1
./day2_1 < day2_input
```

