#include <algorithm>
#include <iostream>

int main() {
    char characterRead;
    int numberOfCharacters = 0;
    std::string marker;
    bool markerFound = false;
    while (not markerFound and std::cin >> characterRead) {
        marker += characterRead;
        if (marker.length() > 4) {
            marker.erase(0, 1); // remove first character
        }
        // check if marker is found (4 unique characters)
        if (marker.length() == 4) {
            std::string copyMarker = marker;
            std::sort(copyMarker.begin(), copyMarker.end());
            markerFound = std::unique(copyMarker.begin(), copyMarker.end()) == copyMarker.end();
        }
        numberOfCharacters++;
    }
    std::cout << numberOfCharacters << std::endl;
    return 0;
}