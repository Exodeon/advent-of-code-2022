#include <iostream>
#include <optional>
#include <unordered_map>

inline int positive_modulo(int i, int n) {
    return (i % n + n) % n;
}

// don't change order !
enum Symbol {ROCK, PAPER, SCISSORS};
const int symbolsNumber = 3;

std::unordered_map<Symbol, int> scoresForSymbol = {
        {Symbol::ROCK, 1},
        {Symbol::PAPER, 2},
        {Symbol::SCISSORS, 3}
};

std::unordered_map<char, Symbol> charToSymbol = {
        {'A', Symbol::ROCK},
        {'B', Symbol::PAPER},
        {'C', Symbol::SCISSORS}
};

enum Result {VICTORY, DEFEAT, DRAW};
std::unordered_map<char, Result> charToResult = {
        {'X', Result::DEFEAT},
        {'Y', Result::DRAW},
        {'Z', Result::VICTORY}
};

std::unordered_map<Result, int> scoresForResult = {
        {Result::DEFEAT, 0},
        {Result::DRAW, 3},
        {Result::VICTORY, 6}
};


std::optional<Symbol> winnerBetween(Symbol opponentPlay, Symbol playerPlay) {
    if (Symbol((opponentPlay + 1) % symbolsNumber) == playerPlay) {
        return playerPlay;
    } else if (Symbol((playerPlay + 1) % symbolsNumber) == opponentPlay) {
        return opponentPlay;
    } else {
        return {};
    }
}

int calculatePlayerScore(Symbol opponentPlay, Symbol playerPlay) {
    int playerScore = scoresForSymbol[playerPlay];
    std::optional<Symbol> winnerSymbol = winnerBetween(opponentPlay, playerPlay);
    if (winnerSymbol.has_value()) {
        if (winnerSymbol.value() == playerPlay) {
            playerScore += 6; // won
        } // no changes to score if we lost
    } else {
        playerScore += 3; // draw
    }
    return playerScore;
}

Symbol getWinningSymbolAgainst(Symbol symbol) {
    return Symbol((symbol + 1) % symbolsNumber);
}

Symbol getLosingSymbolAgainst(Symbol symbol) {
    return Symbol(positive_modulo(symbol - 1, symbolsNumber));
}

int main1() {
    char opponentChar, playerChar;
    int playerScore = 0;
    while (std::cin >> opponentChar >> playerChar) {
        Symbol opponentPlay = charToSymbol[opponentChar];
        Result desiredResult = charToResult[playerChar];
        // TODO improvable : we don't need to recompute who's the winner
        if (desiredResult == Result::VICTORY) {
            playerScore += calculatePlayerScore(opponentPlay, Symbol((opponentPlay + 1) % symbolsNumber));
        } else if (desiredResult == Result::DEFEAT) {
            playerScore += calculatePlayerScore(opponentPlay, Symbol((opponentPlay - 1) % symbolsNumber));
        } else {
            playerScore += calculatePlayerScore(opponentPlay, opponentPlay);
        }
    }
    std::cout << "Score : " << playerScore << std::endl;
    return 0;
}

int main() {
    char opponentChar, resultChar;
    int playerScore = 0;
    while (std::cin >> opponentChar >> resultChar) {
        Symbol opponentPlay = charToSymbol[opponentChar];
        Result desiredResult = charToResult[resultChar];
        // TODO improvable : we don't need to recompute who's the winner
        if (desiredResult == Result::VICTORY) {
            playerScore += scoresForResult[desiredResult] + scoresForSymbol[getWinningSymbolAgainst(opponentPlay)];
        } else if (desiredResult == Result::DEFEAT) {
            playerScore += scoresForResult[desiredResult] + scoresForSymbol[getLosingSymbolAgainst(opponentPlay)];
        } else {
            playerScore += scoresForResult[desiredResult] + scoresForSymbol[opponentPlay];
        }
    }
    std::cout << "Score : " << playerScore << std::endl;
    return 0;
}

