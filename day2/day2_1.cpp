#include <iostream>
#include <optional>
#include <unordered_map>

// don't change order !
enum Symbol {ROCK, PAPER, SCISSORS};
const int symbolsNumber = 3;

std::unordered_map<Symbol, int> scores = {
        {Symbol::ROCK, 1},
        {Symbol::PAPER, 2},
        {Symbol::SCISSORS, 3}
};

std::unordered_map<char, Symbol> charToSymbol = {
        {'A', Symbol::ROCK},
        {'X', Symbol::ROCK},
        {'B', Symbol::PAPER},
        {'Y', Symbol::PAPER},
        {'C', Symbol::SCISSORS},
        {'Z', Symbol::SCISSORS},
};


std::optional<Symbol> winnerBetween(Symbol opponentPlay, Symbol playerPlay) {
    if (Symbol((opponentPlay + 1) % symbolsNumber) == playerPlay) {
        return playerPlay;
    } else if (Symbol((playerPlay + 1) % symbolsNumber) == opponentPlay) {
        return opponentPlay;
    } else {
        return {};
    }
}

int calculatePlayerScore(Symbol opponentPlay, Symbol playerPlay) {
    int playerScore = scores[playerPlay];
    std::optional<Symbol> winnerSymbol = winnerBetween(opponentPlay, playerPlay);
    if (winnerSymbol.has_value()) {
        if (winnerSymbol.value() == playerPlay) {
            playerScore += 6; // won
        } // no changes to score if we lost
    } else {
        playerScore += 3; // draw
    }
    return playerScore;
}

int main() {
    char opponentChar, playerChar;
    int playerScore = 0;
    while (std::cin >> opponentChar >> playerChar) {
        playerScore += calculatePlayerScore(charToSymbol[opponentChar], charToSymbol[playerChar]);
    }
    std::cout << playerScore << std::endl;
}
