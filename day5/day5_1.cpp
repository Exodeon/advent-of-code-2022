#include <iostream>
#include <stack>
#include <vector>
#include <regex>

int main() {
    // example input
    // std::vector<std::stack<char>> stacks(3);

    std::vector<std::stack<char>> stacks(9);

    // TODO : doing input by hand to initialize stacks
    // parsing them properly ?

    // example input
    // stacks.at(0).push('Z');
    // stacks.at(0).push('N');
    //
    // stacks.at(1).push('M');
    // stacks.at(1).push('C');
    // stacks.at(1).push('D');
    //
    // stacks.at(2).push('P');

    stacks.at(0).push('N');
    stacks.at(0).push('S');
    stacks.at(0).push('D');
    stacks.at(0).push('C');
    stacks.at(0).push('V');
    stacks.at(0).push('Q');
    stacks.at(0).push('T');

    stacks.at(1).push('M');
    stacks.at(1).push('F');
    stacks.at(1).push('V');

    stacks.at(2).push('F');
    stacks.at(2).push('Q');
    stacks.at(2).push('W');
    stacks.at(2).push('D');
    stacks.at(2).push('P');
    stacks.at(2).push('N');
    stacks.at(2).push('H');
    stacks.at(2).push('M');

    stacks.at(3).push('D');
    stacks.at(3).push('Q');
    stacks.at(3).push('R');
    stacks.at(3).push('T');
    stacks.at(3).push('F');

    stacks.at(4).push('R');
    stacks.at(4).push('F');
    stacks.at(4).push('M');
    stacks.at(4).push('N');
    stacks.at(4).push('Q');
    stacks.at(4).push('H');
    stacks.at(4).push('V');
    stacks.at(4).push('B');

    stacks.at(5).push('C');
    stacks.at(5).push('F');
    stacks.at(5).push('G');
    stacks.at(5).push('N');
    stacks.at(5).push('P');
    stacks.at(5).push('W');
    stacks.at(5).push('Q');

    stacks.at(6).push('W');
    stacks.at(6).push('F');
    stacks.at(6).push('R');
    stacks.at(6).push('L');
    stacks.at(6).push('C');
    stacks.at(6).push('T');

    stacks.at(7).push('T');
    stacks.at(7).push('Z');
    stacks.at(7).push('N');
    stacks.at(7).push('S');

    stacks.at(8).push('M');
    stacks.at(8).push('S');
    stacks.at(8).push('D');
    stacks.at(8).push('J');
    stacks.at(8).push('R');
    stacks.at(8).push('Q');
    stacks.at(8).push('H');
    stacks.at(8).push('N');

    std::string line;
    const std::regex crateRegex{"move (\\d*) from (\\d*) to (\\d*)"};
    std::smatch match;
    // cin only reads one word, using getline
    while (getline(std::cin,line)) {
        // pass the beginning which was processed by hand
        if (line.starts_with("move")) {
            // get number, start and end
            std::regex_match(line, match, crateRegex);
            int numberToMove = std::stoi(match[1]);
            // in input, stack begin at index 1, so we're taking stack - 1
            int startStack = std::stoi(match[2]) - 1;
            int endStack = std::stoi(match[3]) - 1;
            for (int i = 0; i < numberToMove; i++) {
                int crateToMove = stacks.at(startStack).top();
                stacks.at(startStack).pop();
                stacks.at(endStack).push(crateToMove);
            }
        }
    }
    for (auto& stack : stacks) {
        std::cout << stack.top();
    }
    std::cout << std::endl;
}
