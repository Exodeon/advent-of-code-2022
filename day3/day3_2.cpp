#include <iostream>
#include <vector>

int calculatePriority(char item) {
    if (isupper(item)) {
        // in this priority order, uppercase letters are after lowercase, but that's the opposite table
        // so we're balancing it if it's uppercase
        return item - 'A' + 26 + 1;
    } else {
        return item - 'a' + 1;
    }
}

char findCommonLetter(std::string half1, std::string half2) {
    // O(n^2), improvable in O(n) with maps of frequencies ?
    for (char& currentChar: half1) {
        if (half2.find(currentChar) != std::string::npos) {
            return currentChar;
        }
    }
    return 0;
}


char findCommonLetter3Strings(std::string rucksack1, std::string rucksack2, std::string rucksack3) {
    // find intersection between two first strings
    std::vector<char> vectorRucksSack1(rucksack1.begin(), rucksack1.end());
    std::vector<char> vectorRucksSack2(rucksack2.begin(), rucksack2.end());

    std::sort(std::begin(vectorRucksSack1), std::end(vectorRucksSack1));
    std::sort(std::begin(vectorRucksSack2), std::end(vectorRucksSack2));
    std::vector<char> common_elements;
    std::set_intersection(std::begin(vectorRucksSack1), std::end(vectorRucksSack1)
            , std::begin(vectorRucksSack2), std::end(vectorRucksSack2)
            , std::back_inserter(common_elements));

    // find intersection between first intersection and third string
    std::string string_intersection(common_elements.begin(), common_elements.end());
    return findCommonLetter(string_intersection, rucksack3);
}

int main() {
    std::string rucksack;
    std::vector<std::string> rucksacksGroup;
    int prioritySum = 0;
    while (std::cin >> rucksack) {
        // std::cout << rucksack << std::endl;
        rucksacksGroup.push_back(rucksack);
        if (rucksacksGroup.size() == 3) {
            prioritySum += calculatePriority(findCommonLetter3Strings(rucksacksGroup.at(0), rucksacksGroup.at(1), rucksacksGroup.at(2)));
            rucksacksGroup.clear();
        }
    }

    std::cout << prioritySum << std::endl;

    return 0;
}