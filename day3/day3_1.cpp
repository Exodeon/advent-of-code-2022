#include <iostream>

int calculatePriority(char item) {
    if (isupper(item)) {
        // in this priority order, uppercase letters are after lowercase, but that's the opposite table
        // so we're balancing it if it's uppercase
        return item - 'A' + 26 + 1;
    } else {
        return item - 'a' + 1;
    }
}

char findCommonLetter(std::string half1, std::string half2) {
    // O(n^2), improvable in O(n) with maps of frequencies ?
    for (char& currentChar: half1) {
        if (half2.find(currentChar) != std::string::npos) {
            return currentChar;
        }
    }
    return 0;
}

int main() {
    std::string rucksack;
    int prioritySum = 0;
    while (std::cin >> rucksack) {
        std::string half1 = rucksack.substr(0, rucksack.length() / 2);
        std::string half2 = rucksack.substr(rucksack.length() / 2);
        prioritySum += calculatePriority(findCommonLetter(half1, half2));
    }

    std::cout << prioritySum << std::endl;

    return 0;
}