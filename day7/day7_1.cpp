#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

class Directory;

// Element is either a directory or a file
class Element {
private:
    std::string name;
    std::shared_ptr<Directory> parentDirectory;

public:
    Element(std::string name, std::shared_ptr<Directory> parentDirectory);
    virtual ~Element() = default;
    virtual int getSize() = 0; // if too much memory consumption, store it
    std::shared_ptr<Directory> getParentDirectory();
    std::string getName();
};

Element::Element(std::string name, std::shared_ptr<Directory> parentDirectory) : name{name}, parentDirectory{parentDirectory} {}

std::shared_ptr <Directory> Element::getParentDirectory() {
    return parentDirectory;
}

std::string Element::getName() {
    return name;
}

class Directory : public Element {
private:
    std::vector<std::shared_ptr<Element>> content;

public:
    Directory(std::string name, std::shared_ptr<Directory> parentDirectory);
    virtual int getSize() override;
    std::vector<std::shared_ptr<Element>>& getContent();
    void addContent(std::shared_ptr<Element> element);
    std::shared_ptr<Element> getElementByName(std::string name);
    void printContent();
};

Directory::Directory(std::string name, std::shared_ptr<Directory> parentDirectory) : Element(name, parentDirectory) {}

int Directory::getSize() {
    // TODO use a better function ?
    int size = 0;
    for (auto& element : content) {
        size += element->getSize();
    }
    return size;
}

std::vector<std::shared_ptr<Element>>& Directory::getContent() {
    return content;
}

void Directory::addContent(std::shared_ptr<Element> element) {
    content.push_back(element);
}

std::shared_ptr<Element> Directory::getElementByName(std::string name) {
    auto it = find_if(content.begin(), content.end(), [&name](const std::shared_ptr<Element>& element) {return element->getName() == name;});
    if (it != content.end())
    {
        return *it;
    }
    return nullptr;
}

void Directory::printContent() {
    std::cout << getName() << " contains :" << std::endl;
    for (auto& element : content) {
        std::cout << element->getName() << std::endl;
    }
}

class File : public Element {
private:
    int size;

public:
    File(std::string name, std::shared_ptr<Directory> parentDirectory, int size);
    virtual int getSize() override;
};

File::File(std::string name, std::shared_ptr<Directory> parentDirectory, int size) : Element(name, parentDirectory), size{size} {}

int File::getSize() {
    return size;
}

void getSumOfLittleDirectories(std::shared_ptr<Directory> directory, std::shared_ptr<int> sum) {
    // TODO : clearly non optimal
    int sizeDirectory = directory->getSize();
    if (sizeDirectory <= 100000) {
        *sum += sizeDirectory;
    }
    for (auto& element : directory->getContent()) {
        // TODO dirty
        if (typeid(*element).name() == typeid(Directory).name()) {
            getSumOfLittleDirectories(std::dynamic_pointer_cast<Directory>(element), sum);
        }
    }
}

int main() {
    std::shared_ptr<Directory> rootDirectory = std::make_shared<Directory>("/", nullptr);

    // parse commands and build file tree structure
    std::shared_ptr<Directory> currentDirectory = rootDirectory;
    std::string line;
    // parse first line ("$ cd /")
    getline(std::cin,line);
    while (getline(std::cin,line)) {
        std::stringstream lineStream(line);
        if (line.starts_with("$")) {
            // we did a command
            if (line.starts_with("$ cd")) {
                std::string directoryName;
                // getting directory name which is the last word
                while (lineStream >> directoryName);
                if (directoryName == "..") {
                    currentDirectory = currentDirectory->getParentDirectory();
                } else {
                    currentDirectory = std::dynamic_pointer_cast<Directory>(currentDirectory->getElementByName(directoryName));
                }
            } // for ls, do nothing
        } else {
            // we're listing files of current directory
            if (line.starts_with("dir")) {
                std::string directoryName;
                // getting directory name which is the last word
                while (lineStream >> directoryName);
                std::shared_ptr<Directory> newDirectory = std::make_shared<Directory>(directoryName, currentDirectory);
                currentDirectory->addContent(newDirectory);
            } else {
                // this is a file
                int size;
                std::string name;
                lineStream >> size >> name;
                std::shared_ptr<Element> file = std::make_shared<File>(name, currentDirectory, size);
                currentDirectory->addContent(file);
            }
        }
    }

    // compute size of all directories
    std::shared_ptr<int> sum = std::make_shared<int>(0);
    getSumOfLittleDirectories(rootDirectory, sum);
    std::cout << "Sum = " << *sum << std::endl;


}
